Не стал использовать переменные окружения для ускорения тестирования проекта. 

# Инструкция по запуску
1. `python3 -m venv env` - инициализация виртуального окружения
2. `source env/bin/activate` - вход в виртуальное окружение
3. `pip install -r requirements.txt` - установка зависимостей
4. `docker-compose -f docker-compose.dev.yml up ` - поднятие базы данных (если установлен Docker)
5. `python src/manage.py migrate` - запуск миграций
6. `python src/manage.py runserver` - запуск приложения
7. `celery -A notifier worker -l INFO` - запуск celery worker

## Дополнительные задания
3.подготовить docker-compose для запуска всех сервисов проекта одной командой - на текущий момент ошибка с подключением REDIS

5.сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API.