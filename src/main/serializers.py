from rest_framework import serializers

from main.models import Client, Multicast, MulticastFilter, Message


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class MulticastFilterSerializer(serializers.ModelSerializer):
    class Meta:
        model = MulticastFilter
        fields = "__all__"


class MulticastSerializer(serializers.ModelSerializer):
    filters = MulticastFilterSerializer(many=True)

    def create(self, validated_data):
        filters = [
            MulticastFilter.objects.get_or_create(filter=filter["filter"])[0]
            for filter in validated_data.pop("filters")
        ]
        multicast = Multicast.objects.create(**validated_data)
        multicast.filters.clear()
        multicast.filters.set(filters)

        return multicast

    class Meta:
        model = Multicast
        exclude = ("task_id",)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"
