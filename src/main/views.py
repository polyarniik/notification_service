from django.db.models import Q, Count
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from main.choices import StatusChoices
from main.models import Client, Multicast, Message
from main.serializers import ClientSerializer, MulticastSerializer, MessageSerializer


class ClientModelView(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MulticastModelView(viewsets.ModelViewSet):
    queryset = Multicast.objects.all()
    serializer_class = MulticastSerializer

    @action(detail=False, url_path="statistic/")
    def statistic(self, request):
        messages = (
            Message.objects.order_by("status")
            .annotate(failure_count=Count("status", filter=Q(status=StatusChoices.failure)))
            .annotate(waiting_count=Count("status", filter=Q(status=StatusChoices.waiting)))
            .annotate(success_count=Count("status", filter=Q(status=StatusChoices.success)))
        )
        data = {
            "messagesCount": messages.count(),
            "successCount": messages.success_count,
            "waitingCount": messages.waiting_count,
            "failureCount": messages.failure_count,
            "messages": MessageSerializer(instance=messages, many=True).data,
        }
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=False, url_path="statistic/(?P<multicast_id>[^/.]+)")
    def statistic(self, request, multicast_id):
        messages = (
            Message.objects.filter(multicast_id=multicast_id)
            .order_by("status")
            .annotate(failure_count=Count("status", filter=Q(status=StatusChoices.failure)))
            .annotate(waiting_count=Count("status", filter=Q(status=StatusChoices.waiting)))
            .annotate(success_count=Count("status", filter=Q(status=StatusChoices.success)))
        )
        print(vars(messages))
        data = {
            "messagesCount": messages.count(),
            "successCount": messages.success_count,
            "waitingCount": messages.waiting_count,
            "failureCount": messages.failure_count,
            "messages": MessageSerializer(instance=messages, many=True).data,
        }
        return Response(data, status=status.HTTP_200_OK)
