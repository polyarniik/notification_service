import datetime

import requests
from django.db.models import Q

from main.choices import StatusChoices
from main.models import Multicast, Client, Message
from notifier.celery import app
from notifier.settings import JWT_TOKEN


@app.task()
def _send_notification(client_id, phone, multicast_id, text):
    message, created = Message.objects.get_or_create(
        client_id=client_id,
        multicast_id=multicast_id,
    )
    body = {
        "id": client_id,
        "phone": phone,
        "text": text,
    }
    try:
        response = requests.post(
            "https://probe.fbrq.cloud/v1/send/{}".format(message.id),
            json=body,
            headers={
                "Authorization": f"Bearer {JWT_TOKEN}",
                "accept": "application/json",
                "Content-Type": "application/json",
            },
        )
    except (
            requests.exceptions.Timeout
            or requests.exceptions.ConnectTimeout
            or requests.exceptions.ConnectionError
            or requests.exceptions.RetryError
    ):
        message.status = StatusChoices.waiting
    else:
        message.status = StatusChoices.success if str(response.status_code).startswith("2") else StatusChoices.failure
    if not created:
        message.datetime = datetime.datetime.now()
    return message.save()


@app.task()
def retry_send_notifications(multicast_id):
    multicast = Multicast.objects.get(id=multicast_id)
    clients = Client.objects.filter(
        message__multicast_id=id, message__status__in=[StatusChoices.waiting, StatusChoices.failure]
    )
    print(clients)
    for client in clients:
        _send_notification.delay(client.id, client.phone_number, multicast_id, multicast.text)


@app.task()
def send_notifications(multicast_id):
    multicast = Multicast.objects.get(id=multicast_id)
    filters = multicast.filters.values_list("filter", flat=True)
    clients = Client.objects.filter(Q(phone_code__in=filters) | Q(tag__in=filters))
    for client in clients:
        _send_notification.delay(client.id, client.phone_number, multicast_id, multicast.text)
    retry_send_notifications.apply_async((multicast_id,), eta=datetime.datetime.now() + datetime.timedelta(minutes=5))
