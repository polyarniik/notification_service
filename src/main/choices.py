from djchoices import DjangoChoices, ChoiceItem


class StatusChoices(DjangoChoices):
    success = ChoiceItem("success", label="Success", order=0)
    waiting = ChoiceItem("waiting", label="Waiting", order=1)
    failure = ChoiceItem("failure", label="Failure", order=2)
