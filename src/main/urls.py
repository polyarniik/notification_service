from django.urls import path
from django.views.generic import TemplateView
from rest_framework import routers

from main.views import ClientModelView, MulticastModelView

router = routers.DefaultRouter()
router.register("client", ClientModelView, basename="client")
router.register("multicast", MulticastModelView, basename="multicast")

urlpatterns = []
urlpatterns += router.urls
