from django.core.exceptions import ValidationError
from django.db import models

from main.choices import StatusChoices


class MulticastFilter(models.Model):
    filter = models.CharField(max_length=50)


class Multicast(models.Model):
    start = models.DateTimeField(verbose_name="Начало рассылки")
    end = models.DateTimeField(verbose_name="Конец рассылки")
    text = models.TextField(verbose_name="Текст")
    filters = models.ManyToManyField(MulticastFilter, related_name="multicasts")
    task_id = models.CharField(max_length=100, null=True, blank=True)

    def clean(self):
        if self.start > self.end:
            raise ValidationError("Дата начала рассылки не может быть меньше даты окончания")
        super(Multicast, self).clean()


class Client(models.Model):
    phone_number = models.CharField(max_length=10)
    phone_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=50)
    timezone = models.CharField(max_length=5)


class Message(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=30,
        choices=StatusChoices.choices,
        default=StatusChoices.waiting,
    )
    multicast = models.ForeignKey(Multicast, on_delete=models.DO_NOTHING)
    client = models.ForeignKey(Client, on_delete=models.DO_NOTHING)
