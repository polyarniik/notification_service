import datetime

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver

from main.models import Multicast
from main.tasks import send_notifications
from notifier.celery import app


@receiver(post_save, sender=Multicast, dispatch_uid="update_multicast")
def update_multicast(sender, instance: Multicast, **kwargs):
    print("here")
    multicast = Multicast.objects.get(id=instance.id)
    if multicast.task_id:
        app.control.revoke(multicast.task_id)
    if instance.start.astimezone() <= datetime.datetime.now().astimezone() <= instance.end.astimezone():
        result = send_notifications.delay((instance.id,))
    else:
        result = send_notifications.apply_async(
            (instance.id,),
            eta=instance.start,
            expires=instance.end,
        )
    instance.task_id = result.id

@receiver(pre_delete, sender=Multicast, dispatch_uid="delete_multicast")
def update_multicast(sender, instance, **kwargs):
    multicast = Multicast.objects.get(id=instance.id)
    if multicast.task_id:
        app.control.revoke(multicast.task_id)
